/**
 * Summary (Manejo de peticiones para aplicacion del repartidor de Cliente)
 *
 * Description. (Peticiones manejadas
 *                 Solicitar pedido al restaurante
 *                 Verificar estado del pedido al restaurante
 *                 Verificar estado del pedido al repartidor
 *               )
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
const express = require('express')
const app = express()
const { logger } = require('./services/logger')
var request = require('request')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())
const ESB_HOST = process.env.ESB_HOST || 'localhost'
/**
 *
 * Description. (Envia una orden al restaurante)
 *
 * @since      1.0.0
 * @param {String} nombre    nombre del cliente
*/

app.get('/', (req, res) => {
  const nombre = 'javier'
  var options = {
    method: 'POST',
    url: `http://${ESB_HOST}:6000/recibir`,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: 'javier', estado: 'enviado al restaurante' })
  }
  request(options, function (error, response) {
    let mesg = ''
    if (error) {
      mesg = `Peticion de pedido de Cliente ${nombre} erronea`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
      res.status(400).send(mesg)
    } else {
      mesg = `Pedido de Cliente ${nombre} exitoso`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
      res.status(200).send(mesg)
    }
  })
})

/**
 *
 * Description. (recibe informe de estado de su orden)
 *
 * @since      1.0.0
 * @param {String} nombre    nombre del cliente
*/
app.get('/res', (req, res) => {
  const nombre = 'javier'
  var options = {
    method: 'GET',
    url: `http://${ESB_HOST}:6000/informeRestaurante`,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: 'javier' })
  }
  request(options, function (error, response) {
    let mesg = ''
    if (error) {
      mesg = `Peticion de informe de Cliente ${nombre} erronea`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
    }

    if (response.statusCode === 400) {
      mesg = `Usted ${nombre} no ha realizado ninguna orden al restaurante`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
      res.status(400).send(mesg)
    }

    mesg = `Su orden ${nombre} esta: ${response.body}`
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    res.status(200).send(mesg)
  })
})

/**
 *
 * Description. (recibe informe de estado de su orden del repartidor)
 *
 * @since      1.0.0
 * @param {String} nombre    nombre del cliente
*/

app.get('/rep', (req, res) => {
  const nombre = 'javier'
  var options = {
    method: 'GET',
    url: `http://${ESB_HOST}:6000/informeRepartidor`,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: 'javier' })
  }
  request(options, function (error, response) {
    let mesg = ''
    if (error) {
      mesg = `Peticion de informe de Cliente ${nombre} erronea`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
    }

    if (response.statusCode === 400) {
      mesg = `Usted ${nombre} no ha tiene ninguna orden con el repartidor`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
      res.status(400).send(mesg)
    }

    mesg = `Su orden ${nombre} esta: ${response.body}`
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    res.status(200).send(mesg)
  })
})

app.listen(8000, () => {
  var mesg = 'Servidor de Repartirdor Iniciado en el puerto 8000'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
})
