# Rama  Master

se agregaran los archivos necesarios para trabajar con reverse proxy

# Practica 11 - Reverse Proxy - Docker Compose para Microservicios

se configuraran 4 dockerfile, 1 para cada uno de los microservicios utilizados en la practica y un quinto para nginx como rever proxy

> ESB

> REPARTIDOR

> CLIENTE

> RESTAURANTE

> Nginx

y se levantaran mediante docker-compose para que tabajen bajo un mismo ambiente y puedan seguir utilizandose todo el sistema haciendo uso del puerto 80 nada mas accediendolos por medio de nginx

### Como Utilizar

Estos pasos levantan todo el sistema:
  - Abrir la carpeta raiz que contiene todos los microservicios
  - ejecutar el docker-compose up (ojo sin la bandera de demonio para poder utilizar la consola ya que el cliente la utiliza)
  - esperar a que se levante el docker-compose
  - utlizar la pagina en el puerto:80 del cliente para enviar ordenes
  - utlizar la pagina en el puerto:80/res del cliente para revisar el estado de la orden en el RESTAURANTE
  - utlizar la pagina en el puerto:80/rep del cliente para revisar el estado de la orden en el REPARTIDOR

### Video demostrativo
https://drive.google.com/file/d/18em4hIAdGQHhiXvUGYDCW_MWGQ2FqBI9/view?usp=sharing 




